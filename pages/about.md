---
layout: single
permalink: /about/
title: About Cypher Sex
header:
  image: assets/images/banner_cypher_sex.jpg
---

Cypher Sex is a queer feminist collective aimed at empowering LGBTQ* people, women, and sex workers in their use of online services and digital tools through workshops, guides, and personalized consultancies.

Reach out to cyphersex@lists.riseup.net if you would like to ask for a workshop or a consultancies or if you have other questions.
<br /> We will be happy to help!

If you would like to suggest improvements, please [open an issue](https://0xacab.org/reginazabo/cyphersex-website/-/issues) in our [git project](https://0xacab.org/reginazabo/cyphersex-website), or fork [the repository](https://0xacab.org/reginazabo/cyphersex-website) and do a merge request. If you don't know how this works, please reach out to us by writing to cyphersex@lists.riseup.net.

To create the resources in this website, we have collaborated with several projects - you can find a full list of the people we work with in [this page](../friends/).
