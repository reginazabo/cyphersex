---
layout: single
permalink: /friends/
title: Who we work with
header:
  image: assets/images/banner_cypher_sex.jpg
---

To create the digital security guide for sex workers and other resources in this website, we have collaborated with community organizers, groups for sex workers' rights, illustrators, and more. Here's a complete list, in constant development, of the people we work with.

## Sex Workers Organizations

- [HYDRA e.V. Treffpunkt und Beratung für Prostituierte](https://www.hydra-berlin.de/impressum/) - Berlin


## Artists:

- [Bella Merda Design](https://www.hydra-berlin.de/impressum/)
